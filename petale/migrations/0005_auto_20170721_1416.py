import django.core.validators
from django.db import migrations, models

import petale.models


class Migration(migrations.Migration):

    dependencies = [
        ('petale', '0004_auto_20170330_0046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cut',
            name='uuid',
            field=models.CharField(
                max_length=255, validators=[django.core.validators.RegexValidator('^[A-Za-z0-9-_]+$')]
            ),
        ),
        migrations.AlterField(
            model_name='petal',
            name='data',
            field=models.FileField(
                upload_to=petale.models.petal_directory, max_length=512, verbose_name='Data Content'
            ),
        ),
    ]
