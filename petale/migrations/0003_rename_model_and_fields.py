from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('petale', '0002_auto_20170329_1823'),
    ]

    operations = [
        migrations.RenameModel('CUTIdentifier', 'CUT'),
        migrations.RenameField('Petal', 'partner_id', 'partner'),
        migrations.RenameField('Petal', 'cut_id', 'cut'),
    ]
