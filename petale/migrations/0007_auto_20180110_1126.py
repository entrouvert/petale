import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petale', '0006_auto_20171017_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cut',
            name='uuid',
            field=models.CharField(
                unique=True,
                max_length=255,
                validators=[django.core.validators.RegexValidator('^[A-Za-z0-9-_]+$')],
            ),
        ),
    ]
