import django.core.validators
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessControlList',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('order', models.IntegerField(verbose_name='Order')),
                (
                    'methods',
                    models.CharField(
                        default='GET,PUT,DELETE',
                        help_text='GET, PUT, DELETE',
                        max_length=128,
                        verbose_name='Allowed methods',
                    ),
                ),
                (
                    'key',
                    models.CharField(default='*', max_length=128, verbose_name='Allowed keys', validators=[]),
                ),
            ],
        ),
        migrations.CreateModel(
            name='CUTIdentifier',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'uuid',
                    models.CharField(
                        max_length=32,
                        validators=[
                            django.core.validators.RegexValidator(
                                '^[A-Za-z0-9-_]+$', message='Invalid uuid format'
                            )
                        ],
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name='Partner',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'name',
                    models.CharField(
                        max_length=64,
                        verbose_name='Partner',
                        validators=[
                            django.core.validators.RegexValidator(
                                '^[A-Za-z0-9-_]+$', message='Invalid name format'
                            )
                        ],
                    ),
                ),
                (
                    'admin_emails',
                    models.CharField(
                        help_text='List of admin emails separated by comma',
                        max_length=256,
                        verbose_name='Admin emails',
                    ),
                ),
                ('hard_global_max_size', models.IntegerField(verbose_name='Hard max size')),
                ('soft_global_max_size', models.IntegerField(verbose_name='Soft max size')),
                ('hard_per_key_max_size', models.IntegerField(verbose_name='Hard max size per key')),
                ('soft_per_key_max_size', models.IntegerField(verbose_name='Soft max size per key')),
            ],
        ),
        migrations.CreateModel(
            name='Petal',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                (
                    'name',
                    models.CharField(
                        max_length=128,
                        verbose_name='Name',
                        validators=[
                            django.core.validators.RegexValidator(
                                '^[A-Za-z0-9-_]+$', message='Invalid name format'
                            )
                        ],
                    ),
                ),
                ('etag', models.CharField(max_length=256, verbose_name='ETag', blank=True)),
                ('data', models.FileField(upload_to='data', verbose_name='Data Content', blank=True)),
                ('content_type', models.CharField(max_length=128, verbose_name='Content type', blank=True)),
                ('size', models.IntegerField(default=0, verbose_name='Size')),
                ('cut_id', models.ForeignKey(to='petale.CUTIdentifier', on_delete=models.CASCADE)),
                ('partner_id', models.ForeignKey(to='petale.Partner', on_delete=models.CASCADE)),
            ],
        ),
        migrations.AddField(
            model_name='accesscontrollist',
            name='partner',
            field=models.ForeignKey(to='petale.Partner', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='accesscontrollist',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
        migrations.AlterUniqueTogether(
            name='petal',
            unique_together={('name', 'partner_id', 'cut_id')},
        ),
    ]
