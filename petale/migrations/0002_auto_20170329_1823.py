import django.core.validators
from django.conf import settings
from django.db import migrations, models

import petale.models


class Migration(migrations.Migration):

    dependencies = [
        ('petale', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='size',
            field=models.IntegerField(default=0, verbose_name='Size'),
        ),
        migrations.AlterField(
            model_name='accesscontrollist',
            name='key',
            field=models.CharField(default='*', max_length=128, verbose_name='Allowed keys'),
        ),
        migrations.AlterField(
            model_name='accesscontrollist',
            name='partner',
            field=models.ForeignKey(verbose_name='Partner', to='petale.Partner', on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='accesscontrollist',
            name='user',
            field=models.ForeignKey(
                verbose_name='User', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
            ),
        ),
        migrations.AlterField(
            model_name='cutidentifier',
            name='uuid',
            field=models.CharField(
                max_length=32, validators=[django.core.validators.RegexValidator('^[A-Za-z0-9-_]+$')]
            ),
        ),
        migrations.AlterField(
            model_name='partner',
            name='name',
            field=models.CharField(
                max_length=64,
                verbose_name='Partner',
                validators=[django.core.validators.RegexValidator('^[A-Za-z0-9-_]+$')],
            ),
        ),
        migrations.AlterField(
            model_name='petal',
            name='content_type',
            field=models.CharField(max_length=128, verbose_name='Content type'),
        ),
        migrations.AlterField(
            model_name='petal',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='petal',
            name='data',
            field=models.FileField(upload_to=petale.models.petal_directory, verbose_name='Data Content'),
        ),
        migrations.AlterField(
            model_name='petal',
            name='etag',
            field=models.CharField(max_length=256, verbose_name='ETag'),
        ),
        migrations.AlterField(
            model_name='petal',
            name='name',
            field=models.CharField(
                max_length=128,
                verbose_name='Name',
                validators=[django.core.validators.RegexValidator('^[A-Za-z0-9-_]+$')],
            ),
        ),
        migrations.AlterField(
            model_name='petal',
            name='size',
            field=models.IntegerField(verbose_name='Size'),
        ),
        migrations.AlterField(
            model_name='petal',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Updated'),
        ),
    ]
