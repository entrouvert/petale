from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petale', '0005_auto_20170721_1416'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='hard_global_max_size',
            field=models.IntegerField(default=0, help_text='as kilobytes', verbose_name='Hard max size'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='hard_per_key_max_size',
            field=models.IntegerField(
                default=0, help_text='as kilobytes', verbose_name='Hard max size per key'
            ),
        ),
        migrations.AlterField(
            model_name='partner',
            name='size',
            field=models.BigIntegerField(default=0, help_text='as bytes', verbose_name='Size'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='soft_global_max_size',
            field=models.IntegerField(default=0, help_text='as kilobytes', verbose_name='Soft max size'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='soft_per_key_max_size',
            field=models.IntegerField(
                default=0, help_text='as kilobytes', verbose_name='Soft max size per key'
            ),
        ),
    ]
