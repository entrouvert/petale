import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('petale', '0003_rename_model_and_fields'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='accesscontrollist',
            options={
                'ordering': ['partner__name', 'user__username', 'order', 'key', 'methods'],
                'verbose_name': 'Access control list',
                'verbose_name_plural': 'Access control lists',
            },
        ),
        migrations.AlterModelOptions(
            name='cut',
            options={'verbose_name': 'CUT', 'verbose_name_plural': 'CUTs'},
        ),
        migrations.AlterModelOptions(
            name='partner',
            options={'ordering': ['name'], 'verbose_name': 'Partner', 'verbose_name_plural': 'Partners'},
        ),
        migrations.AlterModelOptions(
            name='petal',
            options={'verbose_name': 'Petal', 'verbose_name_plural': 'Petals'},
        ),
        migrations.AlterField(
            model_name='accesscontrollist',
            name='key',
            field=models.CharField(default='*', max_length=128, verbose_name='Allowed keys'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='admin_emails',
            field=models.CharField(
                help_text='List of admin emails separated by comma',
                max_length=256,
                verbose_name='Admin emails',
                blank=True,
            ),
        ),
        migrations.AlterField(
            model_name='partner',
            name='hard_global_max_size',
            field=models.IntegerField(help_text='as bytes', verbose_name='Hard max size'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='hard_per_key_max_size',
            field=models.IntegerField(help_text='as bytes', verbose_name='Hard max size per key'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='name',
            field=models.CharField(
                unique=True,
                max_length=64,
                verbose_name='Partner',
                validators=[django.core.validators.RegexValidator('^[A-Za-z0-9-_]+$')],
            ),
        ),
        migrations.AlterField(
            model_name='partner',
            name='size',
            field=models.IntegerField(default=0, help_text='as bytes', verbose_name='Size'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='soft_global_max_size',
            field=models.IntegerField(help_text='as bytes', verbose_name='Soft max size'),
        ),
        migrations.AlterField(
            model_name='partner',
            name='soft_per_key_max_size',
            field=models.IntegerField(help_text='as bytes', verbose_name='Soft max size per key'),
        ),
        migrations.AlterField(
            model_name='petal',
            name='size',
            field=models.IntegerField(default=0, help_text='as bytes', verbose_name='Size'),
        ),
    ]
