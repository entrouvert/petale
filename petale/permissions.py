# Petale - Simple App as Key/Value Storage Interface
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from rest_framework.permissions import BasePermission

from .exceptions import AccessForbidden
from .models import AccessControlList


class PetaleAccessPermission(BasePermission):
    def has_permission(self, request, view):
        partner_name = view.kwargs['partner_name']
        petal_name = view.kwargs.get('petal_name')

        qs = AccessControlList.objects.filter(
            partner__name=partner_name, user=request.user, methods__contains=request.method
        )

        if not qs.exists():
            raise AccessForbidden

        if petal_name:
            for acl in qs:
                if acl.key == '*':
                    break
                if petal_name.startswith(acl.key.strip('*')):
                    break
            else:
                raise AccessForbidden
        return True
