# Petale - Simple App as Key/Value Storage Interface
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib import admin

from . import models


class PartnerAdmin(admin.ModelAdmin):
    search_fields = ['admin_emails', 'name']
    list_display = ['name', 'admin_emails']
    readonly_fields = ['size']


admin.site.register(models.Partner, PartnerAdmin)


class CUTAdmin(admin.ModelAdmin):
    search_fields = ['uuid']
    list_display = ['uuid']


admin.site.register(models.CUT, CUTAdmin)


class PetalAdmin(admin.ModelAdmin):
    search_fields = ['partner__name', 'cut__uuid', 'name']
    list_display = ['id', 'partner', 'cut', 'name']
    readonly_fields = ['etag', 'size']
    raw_id_fields = ['cut']


admin.site.register(models.Petal, PetalAdmin)


class AccessControlListAdmin(admin.ModelAdmin):
    search_fields = ['partner__name', 'user__username']
    list_display = ['partner', 'user', 'order', 'methods', 'key']


admin.site.register(models.AccessControlList, AccessControlListAdmin)
