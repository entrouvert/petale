# Petale - Simple App as Key/Value Storage Interface
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from rest_framework import status
from rest_framework.exceptions import APIException


class PetalAPIException(APIException):
    def __init__(self):  # pylint: disable=super-init-not-called
        pass


class NotFound(PetalAPIException):
    status_code = status.HTTP_404_NOT_FOUND


class PartnerNotFound(NotFound):
    detail = {
        'error': 'partner-not-found',
        'description': 'partner identifier is unknown',
    }


class CutNotFound(NotFound):
    status_code = status.HTTP_404_NOT_FOUND
    detail = {
        'error': 'cut-not-found',
        'description': 'cut identifier is unknown',
    }


class KeyNotFound(NotFound):
    status_code = status.HTTP_404_NOT_FOUND
    detail = {
        'error': 'key-not-found',
        'description': 'key identifier is unknown',
    }


class MissingContentType(PetalAPIException):
    status_code = status.HTTP_400_BAD_REQUEST
    detail = {
        'error': 'missing-content-type',
        'description': 'Header Content-Type is missing',
    }


class GlobalSpaceExhausted(PetalAPIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    detail = {
        'error': 'global-space-exhausted',
        'description': 'no more space left for this partner',
    }


class PetalSizeExhausted(PetalAPIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    detail = {
        'error': 'key-space-exhausted',
        'description': 'no more space left for this key',
    }


class ConcurrentAccess(PetalAPIException):
    status_code = status.HTTP_412_PRECONDITION_FAILED
    detail = {
        'error': 'concurrent-access',
        'description': 'data changed since last read',
    }


class AccessForbidden(PetalAPIException):
    status_code = status.HTTP_403_FORBIDDEN
    detail = {
        'error': 'access-forbidden',
        'description': 'Access forbidden',
    }


class PreconditionException(Exception):
    pass
