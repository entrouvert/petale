import django_webtest
import pytest
from utils import (
    create_acl_record,
    create_cut,
    create_partner,
    create_petal,
    create_service,
    get_service,
    get_tests_file_content,
)


@pytest.fixture(autouse=True)
def media(settings, tmp_path):
    media_path = tmp_path / 'media'
    media_path.mkdir()
    settings.MEDIA_ROOT = str(media_path)
    return media_path


@pytest.fixture
def app(request, media):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def cut_kevin_uuid(db):
    return create_cut('a' * 255).uuid


@pytest.fixture
def service_library(db):
    return create_service('library')


@pytest.fixture
def service_family(db):
    return create_service('family')


@pytest.fixture
def service_cityhall(db):
    return create_service('cityhall')


@pytest.fixture
def service_rp(db):
    return create_service(('a1b2' * 8)[:30], password='wrong credentials')


@pytest.fixture
def partner_southpark(service_family, service_library, service_cityhall, service_rp):
    return create_partner('southpark', hg=20240, hk=19728)


@pytest.fixture
def partner_gotham():
    create_service('arkham')
    return create_partner('gotham', admins='b.wayne@gotham.gov', hg=20, sg=10, hk=2, sk=1)


@pytest.fixture
def acl(db, partner_southpark, partner_gotham):
    return [
        create_acl_record(1, partner_southpark, get_service('family'), 'invoice', methods='GET,HEAD,PUT'),
        create_acl_record(2, partner_southpark, get_service('library'), 'books', methods='GET,HEAD'),
        create_acl_record(3, partner_gotham, get_service('arkham'), 'taxes*', methods='GET,HEAD,PUT,DELETE'),
        create_acl_record(
            4, partner_southpark, get_service('library'), 'loans', methods='GET,HEAD,PUT,DELETE'
        ),
        create_acl_record(5, partner_southpark, get_service('library'), 'favourite*', methods='GET,PUT'),
        create_acl_record(
            6, partner_southpark, get_service('cityhall'), 'favourite*', methods='GET,PUT,DELETE'
        ),
        create_acl_record(3, partner_southpark, get_service('library'), 'profile', methods='GET,PUT,DELETE'),
        create_acl_record(1, partner_southpark, get_service('arkham'), 'ma*', methods='GET,HEAD,PUT,DELETE'),
        create_acl_record(
            4, partner_southpark, get_service(('a1b2' * 8)[:30]), 'profile*', methods='GET,PUT'
        ),
    ]


@pytest.fixture
def petal_books(db, cut_kevin_uuid, partner_southpark):
    name = 'books'
    content_type = 'application/json'
    data = b'{"books": [{"1984": "U all doomed"}, {"Candide": "Another naive boy"}]}'
    return create_petal(cut_kevin_uuid, partner_southpark, name, data, content_type)


@pytest.fixture
def petal_invoice(db, cut_kevin_uuid, partner_southpark):
    name = 'invoice-20170220'
    content_type = 'application/pdf'
    data = get_tests_file_content('invoice.pdf')
    return create_petal(cut_kevin_uuid, partner_southpark, name, data, content_type)
