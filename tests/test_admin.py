import pytest


@pytest.fixture
def admin(db):
    from django.contrib.auth.models import User

    user = User.objects.create(username='admin', email='admin@example.com', is_superuser=True, is_staff=True)
    user.set_password('admin')
    user.save()
    return user


def test_create_user(settings, app, db, admin):
    from django.contrib.auth.models import User

    settings.PETALE_CHECK_CUT_UUID = False

    # Login
    response = app.get('/admin/').follow()
    response.form.set('username', 'admin')
    response.form.set('password', 'admin')
    response = response.form.submit().follow()

    # Ajout d'un utilisateur pour le partenaire
    response = response.click('Utilisateurs')
    response = response.click('Ajouter utilisateur')
    response.form.set('username', 'user-partenaire1')
    response.form.set('password1', 'user-partenaire1')
    response.form.set('password2', 'user-partenaire1')
    response = response.form.submit().follow()
    response = response.click('Accueil')

    assert User.objects.get(username='user-partenaire1').check_password('user-partenaire1')

    # Création du partenaire
    response = response.click('Partenaire')
    response = response.click('Ajouter Partenaire')
    response.form.set('name', 'partenaire1')
    response.form.set('hard_global_max_size', '1000000')
    response.form.set('soft_global_max_size', '500000')
    response.form.set('hard_per_key_max_size', '10')
    response.form.set('hard_per_key_max_size', '10')
    response.form.set('soft_per_key_max_size', '5')
    response = response.form.submit().follow()
    response = response.click('Accueil')

    # Création de la règle de contrôle d'accès
    response = response.click('Règles de cont')
    response = response.click('Ajouter Règle')
    response.form.set('order', '1')
    response.form.select('partner', text='partenaire1')
    response.form.select('user', text='user-partenaire1')
    response = response.form.submit().follow()
    response = response.click('Accueil')
    response = response.click('Déconnexion')

    app.authorization = ('Basic', ('user-partenaire1', 'user-partenaire1'))
    response = app.put_json('/api/partenaire1/cut1/cle1/', params={'coucou': 1})
    assert response.json == {}

    response = app.get('/api/partenaire1/cut1/')
    assert response.json == {'keys': ['cle1']}

    response = app.get('/api/partenaire1/cut1/cle1/')
    assert response.json == {'coucou': 1}

    app.authorization = None

    # Login
    response = app.get('/admin/').follow()
    response.form.set('username', 'admin')
    response.form.set('password', 'admin')
    response = response.form.submit().follow()

    response = response.click('Pétales')
    row = [e.text() for e in response.pyquery('tbody tr > *').items()]
    assert row[:1] + row[2:] == ['', 'partenaire1', 'cut1', 'cle1']
    response = response.click(row[1])
    assert response.pyquery('[name="name"][value="cle1"]')
    assert response.pyquery('.field-cut strong').text() == 'cut1'
