import os

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'petale',
    }
}

if 'postgres' in DATABASES['default']['ENGINE']:
    for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
        if key in os.environ:
            DATABASES['default'][key[2:]] = os.environ[key]

PETALE_AUTHENTIC_URL = 'http://example.net/idp/'
PETALE_AUTHENTIC_AUTH = ('foo', 'bar')
MEDIA_ROOT = '/does-not-exist/'
