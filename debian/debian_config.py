# This file is sourced by "execfile" from petale.settings

import glob
import os

from django.core.exceptions import ImproperlyConfigured

PROJECT_NAME = 'petale'

#
# local settings
#

ETC_DIR = '/etc/%s' % PROJECT_NAME
VAR_DIR = '/var/lib/%s' % PROJECT_NAME

# collecstatic destination
STATIC_ROOT = os.path.join(VAR_DIR, 'collectstatic')

# media destination
MEDIA_ROOT = os.path.join(VAR_DIR, 'media')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': PROJECT_NAME.replace('-', '_'),
    }
}

ADMINS = (('Tous', 'root@localhost'),)

EMAIL_SUBJECT_PREFIX = '[%s] ' % PROJECT_NAME

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'syslog': {
            'format': '%(levelname)s %(name)s.%(funcName)s: %(message)s',
        },
        'syslog_no_filter': {
            'format': '%(levelname)s %(name)s.%(funcName)s: %(message)s',
        },
    },
    'handlers': {
        'syslog': {
            'level': 'DEBUG',
            'address': '/dev/log',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'syslog',
        },
        'syslog_no_filter': {
            'level': 'DEBUG',
            'address': '/dev/log',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'syslog_no_filter',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {
        'django': {
            # Override Django default values
            'handlers': [],
            'level': 'NOTSET',
            'propagate': True,
        },
        'django.server': {
            # Override Django 1.8 default values
            'handlers': [],
            'level': 'NOTSET',
            'propagate': True,
        },
        'django.request': {
            # Override Django default values
            'handlers': [],
            'level': 'NOTSET',
            'propagate': True,
        },
        'django.security': {
            # Override Django default values
            'handlers': [],
            'level': 'NOTSET',
            'propagate': True,
        },
        # log py.warnings to syslog
        'py.warnings': {
            'handlers': ['syslog_no_filter'],
            'level': 'WARNING',
            'propagate': False,
        },
        '': {
            'level': 'INFO',
            'handlers': ['syslog', 'mail_admins'],
        },
    },
}

# Graylog support
if 'GRAYLOG_URL' in os.environ:
    try:
        from graypy import GELFHandler
    except ImportError:
        raise ImproperlyConfigured('cannot configure graypy, import of GELFHandler failed')
    else:
        host = os.environ['GRAYLOG_URL'].split(':')[0]
        port = int(os.environ['GRAYLOG_URL'].split(':')[1])
        LOGGING['handlers']['gelf'] = {
            'class': 'graypy.GELFHandler',
            'fqdn': True,
            'host': host,
            'port': port,
        }
        LOGGING['loggers']['']['handlers'].append('gelf')

main_settings_py = os.path.join(ETC_DIR, 'settings.py')

for filename in [main_settings_py] + sorted(glob.glob(os.path.join(ETC_DIR, 'settings.d', '*.py'))):
    with open(filename) as fd:
        exec(fd.read())
