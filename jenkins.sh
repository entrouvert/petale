#!/bin/sh

set -e

rm -f coverage.xml
rm -f test_results.xml

pip install --upgrade Django\<1.9
pip install --upgrade tox
tox -r
